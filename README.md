# pic-thermostat #
  
PIC-based heating thermostat with advanced functions and SSD1306-based OLED display.  

Features:  

* 7-days schedule with half hour steps
* Adjustable daytime and night time temperatures with 0.1 Celsius resolution
* Adjustable active temperature (without modifying daytime and night time values) with 0.1 Celsius resolution
* Adjustable temperature overshoot and undershoot with 0.1 Celsius resolution
* Special BOOST function:
  * 1-click activation for instant short-term heating
  * 1-click heating time expansion
  * Adjustable expansion amount
* Easy navigation with 6 buttons
* Easy to use menu system
* Adjustable display brightness

IoT capabilities with ESP8266-based back-end module using Blynk:

* Monitor current state
* Control BOOST state and temperature
* Additional terminal for diagnosis
* Simple UART communication with the front-end

---

![pic_front.jpg](https://bitbucket.org/repo/x45rEa/images/3724759567-pic_front.jpg)
![pic_display.jpg](https://bitbucket.org/repo/x45rEa/images/340896571-pic_display.jpg)