/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.

    Author: Tamas Karpati
    Created on 2017-01-09
*/

#include "settings.h"
#include "mcc_generated_files/mcc.h"
#include "heat_ctl.h"

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#define EEPROM_BASE	(size_t)(0x7000u)
#define EEPROM_ADDR(x)	(EEPROM_BASE + x)

struct persistent_settings settings;

static void check_settings();

void settings_load()
{
	uint8_t* data = (uint8_t*)&settings;
	const uint8_t* eedata = (uint8_t*)EEPROM_BASE;

	// Load data from EEPROM via FSR access
	memcpy(data, eedata, sizeof(struct persistent_settings));

	check_settings();
}

void settings_save()
{
	const uint8_t* data = (uint8_t*)&settings;
	const uint8_t* eedata = (uint8_t*)EEPROM_BASE;

	for (uint8_t i = 0; i < sizeof(struct persistent_settings); ++i) {
		// Spare some write cycles by storing only the changed bytes
		if (*data != *eedata) {
			DATAEE_WriteByte(EEPROM_ADDR(i), *data);
		}

		++data;
		++eedata;
	}
}

void settings_save_heatctl()
{
	const uint8_t* data = (uint8_t*)&settings.heatctl;

	// Probably a compiler bug, but these must be volatile in order to
	// calculate them properly
	volatile size_t offset = offsetof(struct persistent_settings, heatctl);
	volatile size_t address = EEPROM_BASE + offset;

	const uint8_t* eedata = (uint8_t*)(address);

	for (uint8_t i = 0; i < sizeof(struct _settings_heatctl); ++i) {
		// Spare some write cycles by storing only the changed bytes
		if (*data != *eedata) {
			DATAEE_WriteByte(EEPROM_ADDR(i), *data);
		}

		++data;
		++eedata;
	}
}

static void check_settings()
{
	bool modified = false;

	// Reset Heat Control mode if it's corrupted
	if (settings.heatctl.mode > HC_MODE_OFF) {
		settings.heatctl.mode = HC_MODE_OFF;
		modified = true;
	}

	// If daytime temp is out of range, reset to default
	if (settings.heatctl.day_temp > SETTINGS_LIMIT_HEATCTL_DAY_TEMP_MAX || settings.heatctl.day_temp < SETTINGS_LIMIT_HEATCTL_DAY_TEMP_MIN) {
		settings.heatctl.day_temp = SETTINGS_DEFAULT_HEATCTL_DAY_TEMP;
		modified = true;
	}

	// If nighttime temp is out of range, reset to default
	if (settings.heatctl.night_temp > SETTINGS_LIMIT_HEATCTL_NIGHT_TEMP_MAX || settings.heatctl.night_temp < SETTINGS_LIMIT_HEATCTL_NIGHT_TEMP_MIN) {
		settings.heatctl.night_temp = SETTINGS_DEFAULT_HEATCTL_NIGHT_TEMP;
		modified = true;
	}

	// If temperature overshoot is out of range, reset to default
	if (settings.heatctl.overshoot > SETTINGS_LIMIT_HEATCTL_OVERSHOOT_MAX || settings.heatctl.night_temp < SETTINGS_LIMIT_HEATCTL_OVERSHOOT_MIN) {
		settings.heatctl.overshoot = SETTINGS_DEFAULT_HEATCTL_OVERSHOOT;
		modified = true;
	}

	// If temperature undershoot is out of range, reset to default
	if (settings.heatctl.undershoot > SETTINGS_LIMIT_HEATCTL_UNDERSHOOT_MAX || settings.heatctl.undershoot < SETTINGS_LIMIT_HEATCTL_UNDERSHOOT_MIN) {
		settings.heatctl.undershoot = SETTINGS_DEFAULT_HEATCTL_UNDERSHOOT;
		modified = true;
	}

	// If temperature correction is out of range, reset to default
	if (settings.heatctl.temp_correction > SETTINGS_LIMIT_HEATCTL_TEMP_CORR_MAX || settings.heatctl.temp_correction < SETTINGS_LIMIT_HEATCTL_TEMP_CORR_MIN) {
		settings.heatctl.undershoot = SETTINGS_DEFAULT_HEATCTL_TEMP_CORR;
		modified = true;
	}

	// If BOOST interval is out of range, reset to default
	if (settings.heatctl.boost_intval > SETTINGS_LIMIT_HEATCTL_BOOST_INTVAL_MAX || settings.heatctl.boost_intval < SETTINGS_LIMIT_HEATCTL_BOOST_INTVAL_MIN) {
		settings.heatctl.boost_intval = SETTINGS_DEFAULT_HEATCTL_BOOST_INTVAL;
		modified = true;
	}

        // If Custom Temperature Timeout is out of range, reset to default
        if (settings.heatctl.custom_temp_timeout > SETTINGS_LIMIT_HEATCTL_CUSTOM_TEMP_TIMEOUT_MAX || settings.heatctl.custom_temp_timeout < SETTINGS_LIMIT_HEATCTL_CUSTOM_TEMP_TIMEOUT_MIN) {
                settings.heatctl.custom_temp_timeout = SETTINGS_DEFAULT_HEATCTL_CUSTOM_TEMP_TIMEOUT;
                modified = true;
        }

	// If there was a correction, assume that the settings data is
	// corrupted, so reset the brightness of the display to default.
	// This check is necessary since all possible values (0-255) are valid
	// for backlight level thus we cannot decide if it's corrupted or not.
	// At last, save the corrected values.
	if (modified) {
		settings.display.brightness = SETTINGS_DEFAULT_DISPLAY_BRIGHTNESS;
		settings.display.timeout_secs = SETTINGS_DEFAULT_DISPLAY_TIMEOUT_SECS;
		settings_save();
	}
}