/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.
    
    Author: Tamas Karpati
    Created on 2017-01-07
*/

#ifndef SCHEDULING_SCREEN_H
#define	SCHEDULING_SCREEN_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "ui_result.h"
#include <stdint.h>
	
void scheduling_screen_init();
void scheduling_screen_draw();
ui_result scheduling_screen_handle_keys(uint16_t keys);

#ifdef	__cplusplus
}
#endif

#endif	/* SCHEDULING_SCREEN_H */

