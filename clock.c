/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.

    Author: Tamas Karpati
    Created on 2016-12-30
*/

#include "clock.h"
#include "mcc_generated_files/tmr1.h"

time_t clock_epoch = 0;
bool clock_synced = false;

static void handle_tick();

void clock_init()
{
	TMR1_SetInterruptHandler(handle_tick);
}

static void handle_tick()
{
	++clock_epoch;
}