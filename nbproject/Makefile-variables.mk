#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# simulator configuration
CND_ARTIFACT_DIR_simulator=dist/simulator/production
CND_ARTIFACT_NAME_simulator=pic-thermostat.production.hex
CND_ARTIFACT_PATH_simulator=dist/simulator/production/pic-thermostat.production.hex
CND_PACKAGE_DIR_simulator=${CND_DISTDIR}/simulator/package
CND_PACKAGE_NAME_simulator=pic-thermostat.tar
CND_PACKAGE_PATH_simulator=${CND_DISTDIR}/simulator/package/pic-thermostat.tar
# debug configuration
CND_ARTIFACT_DIR_debug=dist/debug/production
CND_ARTIFACT_NAME_debug=pic-thermostat.production.hex
CND_ARTIFACT_PATH_debug=dist/debug/production/pic-thermostat.production.hex
CND_PACKAGE_DIR_debug=${CND_DISTDIR}/debug/package
CND_PACKAGE_NAME_debug=pic-thermostat.tar
CND_PACKAGE_PATH_debug=${CND_DISTDIR}/debug/package/pic-thermostat.tar
# release configuration
CND_ARTIFACT_DIR_release=dist/release/production
CND_ARTIFACT_NAME_release=pic-thermostat.production.hex
CND_ARTIFACT_PATH_release=dist/release/production/pic-thermostat.production.hex
CND_PACKAGE_DIR_release=${CND_DISTDIR}/release/package
CND_PACKAGE_NAME_release=pic-thermostat.tar
CND_PACKAGE_PATH_release=${CND_DISTDIR}/release/package/pic-thermostat.tar
