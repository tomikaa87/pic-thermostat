/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.
    
    Author: Tamas Karpati
    Created on 2017-01-12
*/

#include "CLI.h"
#include "BlynkHandler.h"

#include <Arduino.h>
#include <NTPClient.h>
#include <ctype.h>
#include <string.h>
#include <TimeLib.h>

//#define ENABLE_DEBUG

#ifdef ENABLE_DEBUG
#define PARSER_DEBUG(__MSG) Serial.print("<" __MSG ">")
#define PARSER_DEBUGF(__FMT, ...) Serial.printf("<" __FMT ">", __VA_ARGS__)
#else
#define PARSER_DEBUG(__MSG)
#define PARSER_DEBUGF(__FMT, ...)
#endif

/*
*	Thermostat packet format
*	;;<active temp 0-ffff>:
*	<current temp 0-ffff>:
*	<daytime temp 0-ffff>:
*	<night time temp 0-ffff>:
*	<boost state 0-1>:
*	<heating state 0-1>:
*	<boost remaining secs 0-ffffffff>:
*	<mode 0-2>:
*	<next switch state 0-1>:
*	<next switch weekday 0-ff>:
*	<next switch state 0-ff>:
*	<next switch state 0-ff>:
*	<time synced 0-1>;
*/

/*
 * Log message format:
 * ## custom log message #
 */

CLI::CLI(BlynkHandler& blynk, NTPClient& ntpClient)
    : m_blynk(blynk)
    , m_ntpClient(ntpClient)
{
    clearBuffer();
    setupBlynkHandler();
}

void CLI::task()
{
    while (Serial.available())
        processInput(static_cast<char>(Serial.read()));

    static const uint32_t ntpUpdateInterval = 5ul * 60ul * 1000ul;
    if (m_lastNtpUpdate == 0 || millis() - m_lastNtpUpdate >= ntpUpdateInterval || m_thermostatClockUpdateNeeded)
    {
        if (m_ntpClient.forceUpdate())
        {
            m_lastNtpUpdate = millis();
            updateThermostatClock();
        }
        else
        {
            // Try again 10 seconds later
            m_lastNtpUpdate = millis() - ntpUpdateInterval + 10000ul;
        }
    }
}

void CLI::updateThermostatClock()
{
#ifdef ENABLE_DEBUG
    Serial.println("# CLI::updateThermostatClock()");
#endif // ENABLE_DEBUG

    m_thermostatClockUpdateNeeded = false;

    unsigned long localTime = m_ntpClient.getEpochTime();
    if (isDST(localTime))
        localTime += 3600;

    char buf[16] = { 0 };
    snprintf(buf, sizeof(buf), "%lu", localTime);
    sendThermostatCommand(Command::SetTime, buf);
}

void CLI::sendThermostatCommand(Command command, const char* param) const
{
    Serial.print(";;");
    Serial.print(static_cast<char>(command));

    if (param)
        Serial.print(param);

    Serial.print(";");
}

void CLI::processInput(char c)
{
    PARSER_DEBUGF("In: %c", c);

    switch (m_inputProcState)
    {
    case CLI::InputProcessorState::Init:
        if (c == ';' || c == '#')
        {
            PARSER_DEBUG("Init");
            clearBuffer();

            if (c == ';')
            {
                PARSER_DEBUG("ReadStatusPacket");
                m_inputProcState = InputProcessorState::ReadStatusPacket;
            }
            else if (c == '#')
            {
                PARSER_DEBUG("ReadLogMessage");
                m_inputProcState = InputProcessorState::ReadLogMessage;
            }
        }
        break;

    case CLI::InputProcessorState::ReadStatusPacket:
        if (c == ';')
        {
            if (m_bufIdx == 0)
            {
                PARSER_DEBUG("RSP: skip");
            }
            else
            {
                PARSER_DEBUG("RSP: process");
                m_inputProcState = InputProcessorState::Init;
                parseStatusPacket();
            }

            break;
        }
        if ((!isalnum(c) && c != ':') || m_bufIdx >= sizeof(m_buffer) - 1)
        {
            PARSER_DEBUG("RSP: Invalid char");
            m_inputProcState = InputProcessorState::Init;
            break;
        }
        m_buffer[m_bufIdx++] = c;
        PARSER_DEBUGF("RSP: BufIdx: %u", m_bufIdx);
        break;
        
    case CLI::InputProcessorState::ReadLogMessage:
        if (c == '#')
        {
            if (m_bufIdx == 0)
            {
                PARSER_DEBUG("RLM: skip");
            }
            else
            {
                PARSER_DEBUG("RLM: process");
                m_inputProcState = InputProcessorState::Init;
                processLogMessage();
            }

            break;
        }
        if (m_bufIdx >= sizeof(m_buffer) - 1)
        {
            PARSER_DEBUG("RSP: buffer full");
            m_inputProcState = InputProcessorState::Init;
            processLogMessage();
        }
        m_buffer[m_bufIdx++] = c;
        PARSER_DEBUGF("RLM: BufIdx: %u", m_bufIdx);
        break;
    }
}

void CLI::parseStatusPacket()
{
    if (strlen(m_buffer) == 0)
        return;

    char* end = nullptr;

    auto token = strtok(m_buffer, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("7");
#endif // ENABLE_DEBUG
        return;
    }
    uint16_t activeTemp = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<activeTemp=%u>", activeTemp);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<8>");
#endif // ENABLE_DEBUG
        return;
    }
    uint16_t currentTemp = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<currentTemp=%u>", currentTemp);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<8>");
#endif // ENABLE_DEBUG
        return;
    }
    uint16_t daytimeTemp = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<daytimeTemp=%u>", daytimeTemp);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<9>");
#endif // ENABLE_DEBUG
        return;
    }
    uint16_t nightTimeTemp = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<nightTimeTemp=%u>", nightTimeTemp);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<10>");
#endif // ENABLE_DEBUG
        return;
    }
    bool boostActive = strtol(token, &end, 10) == 1 ? true : false;
#ifdef ENABLE_DEBUG
    Serial.printf("<boostActive=%u>", boostActive);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<11>");
#endif // ENABLE_DEBUG
        return;
    }
    bool heatingActive = strtol(token, &end, 10) == 1 ? true : false;
#ifdef ENABLE_DEBUG
    Serial.printf("<heatingActive=%u>", heatingActive);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<12>");
#endif // ENABLE_DEBUG
        return;
    }
    uint32_t boostRemainingSecs = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<boostRemainingSecs=%u>", boostRemainingSecs);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<13>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t mode = strtol(token, &end, 10);
#ifdef ENABLE_DEBUG
    Serial.printf("<mode=%u>", mode);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<14>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t nextState = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<nextState=%u>", nextState);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<15>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t nextWeekday = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<nextWeekday=%u>", nextState);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<16>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t nextHour = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<nextHour=%u>", nextState);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<17>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t nextMinute = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<nextMinute=%u>", nextState);
#endif // ENABLE_DEBUG

    token = strtok(nullptr, ":");
    if (!token)
    {
#ifdef ENABLE_DEBUG
        Serial.print("<18>");
#endif // ENABLE_DEBUG
        return;
    }
    uint8_t clockSynced = strtol(token, &end, 16);
#ifdef ENABLE_DEBUG
    Serial.printf("<clockSynced=%u>", clockSynced);
    Serial.print("<end>");
#endif // ENABLE_DEBUG

    if (clockSynced == 0)
        m_thermostatClockUpdateNeeded = true;

    m_blynk.updateActiveTemperature(static_cast<float>(activeTemp) / 10.f);
    m_blynk.updateCurrentTemperature(static_cast<float>(currentTemp) / 10.f);
    m_blynk.updateDaytimeTemperature(static_cast<float>(daytimeTemp) / 10.f);
    m_blynk.updateNightTimeTemperature(static_cast<float>(nightTimeTemp) / 10.f);
    m_blynk.updateIsBoostActive(boostActive);
    m_blynk.updateIsHeatingActive(heatingActive);
    m_blynk.updateBoostRemaining(boostRemainingSecs);
    m_blynk.updateMode(mode);
    m_blynk.updateNextSwitch(nextState, nextWeekday, nextHour, nextMinute);
}

void CLI::processLogMessage()
{
    if (strlen(m_buffer) == 0)
        return;

    time_t epoch = m_ntpClient.getEpochTime();
    tmElements_t tme;

    breakTime(epoch, tme);

    char msg[sizeof(m_buffer) + 64] = { 0 };

    sprintf(msg, "%04u-%02u-%02u %02u:%02u:%02u: %s",
        tme.Year + 1970, tme.Month, tme.Day, tme.Hour, tme.Minute, tme.Second,
        m_buffer);

    m_blynk.terminalPrintln(msg);
}

void CLI::clearBuffer()
{
    memset(m_buffer, 0, sizeof(m_buffer));
    m_bufIdx = 0;
}

void CLI::setupBlynkHandler()
{
    m_blynk.setIncrementTempCallback([this] {
#ifdef ENABLE_DEBUG
        Serial.print("CMD: IncrementTemp\r\n");
#endif // ENABLE_DEBUG
        sendThermostatCommand(Command::IncrementTemperature);
    });

    m_blynk.setDecrementTempCallback([this] {
#ifdef ENABLE_DEBUG
        Serial.print("CMD: DecrementTemp\r\n");
#endif // ENABLE_DEBUG
        sendThermostatCommand(Command::DecrementTemperature);
    });

    m_blynk.setActivateBoostCallback([this] {
#ifdef ENABLE_DEBUG
        Serial.print("CMD: ActivateBoost\r\n");
#endif // ENABLE_DEBUG
        sendThermostatCommand(Command::ActivateBoost);
    });

    m_blynk.setDeactivateBoostCallback([this] {
#ifdef ENABLE_DEBUG
        Serial.print("CMD: DeactivateBoost\r\n");
#endif // ENABLE_DEBUG
        sendThermostatCommand(Command::DeactivateBoost);
    });

    m_blynk.setTargetTemperatureChangedCallback([this](float celsius) {
#ifdef ENABLE_DEBUG
        Serial.printf("CMD: SetTargetTemperature: %f\r\n", celsius);
#endif // ENABLE_DEBUG
        char buf[10] = { 0 };
        snprintf(buf, sizeof(buf) - 1, "%d", static_cast<int>(celsius * 10));
        sendThermostatCommand(Command::SetTargetTemperature, buf);
    });

    m_blynk.setNightTimeTemperatureChangedCallback([this](float celsius) {
#ifdef ENABLE_DEBUG
        Serial.printf("CMD: SetNightTimeTemperature: %f\r\n", celsius);
#endif // ENABLE_DEBUG
        char buf[10] = { 0 };
        snprintf(buf, sizeof(buf) - 1, "%d", static_cast<int>(celsius * 10));
        sendThermostatCommand(Command::SetNightTimeTemperature, buf);
    });

    m_blynk.setDaytimeTemperatureChangedCallback([this](float celsius) {
#ifdef ENABLE_DEBUG
        Serial.printf("CMD: SetDaytimeTemperature: %f\r\n", celsius);
#endif // ENABLE_DEBUG
        char buf[10] = { 0 };
        snprintf(buf, sizeof(buf) - 1, "%d", static_cast<int>(celsius * 10));
        sendThermostatCommand(Command::SetDaytimeTemperature, buf);
    });

    m_blynk.setModeChangedCallback([this](uint8_t mode) {
#ifdef ENABLE_DEBUG
        Serial.printf("CMD: SetMode: %u\r\n", mode);
#endif // ENABLE_DEBUG
        char buf[4] = { 0 };
        snprintf(buf, sizeof(buf) - 1, "%u", mode);
        sendThermostatCommand(Command::SetMode, buf);
    });
}

bool CLI::isDST(unsigned long epochTime)
{
    tmElements_t t;
    breakTime(epochTime, t);

    // This should never happen since breakTime() fills this field properly
    if (t.Wday == 0)
        return false;

    int month = t.Month;

    if (month < 3 || month > 10)
        return false;

    if (month > 3 && month < 10)
        return true;

    // TimeLib provides a different Wday where Sunday has a value of 1 instead of 0
    int previousSunday = t.Day - (t.Wday - 1);

    if (month == 3)
        return previousSunday >= 25;

    if (month == 10)
        return previousSunday < 25;

    // This should never happen
    return false;
}
