/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.
    
    Author: Tamas Karpati
    Created on 2017-01-12
*/

#pragma once

#include <stdint.h>

class BlynkHandler;
class NTPClient;

class CLI
{
public:
    CLI(BlynkHandler& blynk, NTPClient& ntpClient);

    void task();

    void updateThermostatClock();

    enum class Command : char
    {
        SetTime = 'A',
        IncrementTemperature,
        DecrementTemperature,
        ActivateBoost,
        DeactivateBoost,
        SetMode,
        SetTargetTemperature,
        SetNightTimeTemperature,
        SetDaytimeTemperature
    };

    void sendThermostatCommand(Command command, const char* param = nullptr) const;

private:
    char m_buffer[128];
    uint8_t m_bufIdx = 0;
    BlynkHandler& m_blynk;
    NTPClient& m_ntpClient;
    uint32_t m_lastNtpUpdate = 0;
    bool m_thermostatClockUpdateNeeded = false;

    void clearBuffer();

    enum class InputProcessorState 
    {
        Init,
        ReadStatusPacket,
        ReadLogMessage
    };

    InputProcessorState m_inputProcState = InputProcessorState::Init;

    void processInput(char c);
    void parseStatusPacket();
    void processLogMessage();

    void setupBlynkHandler();

    static bool isDST(unsigned long epochTime);
};