/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.
    
    Author: Tamas Karpati
    Created on 2017-01-12
*/

#include <Arduino.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// This header must contain the following:
//      static const char* BlynkAppToken = "<token>";
//      static const char* WifiSSID = "<wifi ap ssid>";
//      static const char* WifiPassword = "<wifi ap password>";
#include "PrivateConfig.h"

#include "BlynkHandler.h"
#include "CLI.h"

BlynkHandler* g_blynk = nullptr;
CLI* g_cli = nullptr;
NTPClient* g_ntp = nullptr;

void setupNtp()
{
    static const int timeOffsetMinutes = 60;
    static const int updateIntervalMinutes = 60;

    static WiFiUDP udpSocket;
    static NTPClient ntpCli{ udpSocket, "login-vlan194.budapest2.hpc.niif.hu", timeOffsetMinutes * 60, updateIntervalMinutes * 60000 };
    g_ntp = &ntpCli;
    g_ntp->begin();
    g_ntp->forceUpdate();
}

void setup()
{
    Serial.begin(115200);

    static BlynkHandler blynkHandler{
        PrivateConfig::BlynkApiToken,
        PrivateConfig::WiFiSSID,
        PrivateConfig::WiFiPassword
    };

    g_blynk = &blynkHandler;

    setupNtp();

    static CLI cli{ blynkHandler, *g_ntp };
    g_cli = &cli;
}

void loop()
{
    if (g_blynk)
        g_blynk->task();

    if (g_cli)
        g_cli->task();
}