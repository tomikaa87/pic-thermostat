/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.

    Author: Tamas Karpati
    Created on 2016-12-01
*/

#define SW_VERSION	"2.1.0"

#include "mcc_generated_files/mcc.h"
#include "clock.h"
#include "cli.h"
#include "heat_ctl.h"
#include "keypad.h"
#include "ui.h"
#include "settings.h"
#include "ds18x20.h"

#include <stdio.h>

void main(void)
{
	SYSTEM_Initialize();
	GIE = 1;
	PEIE = 1;

	// Let the hardware power up properly
#ifndef DEBUG
	__delay_ms(10);
#endif

	settings_load();
	keypad_init();
	clock_init();
	cli_init();
	heatctl_init();
	ui_init();

	cli_log("SW: " SW_VERSION);
	
	uint32_t last_epoch = clock_epoch;

	// Main loop
	for (;;) {
		if (last_epoch != clock_epoch) {
			last_epoch = clock_epoch;
			ds18x20_update();
			heatctl_task();
			ui_update();
			cli_update_backend();
		}
		
		cli_task();
		
		uint16_t pressed_keys = keypad_task();
		ui_handle_keys(pressed_keys);
		
		CLRWDT();
	}
}
