/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.
    
    Author: Tamas Karpati
    Created on 2017-01-01
*/

#include "one_wire.h"
#include "mcc_generated_files/mcc.h"

#define OW_FLOAT()	ONE_WIRE_DQ_SetDigitalInput()
#define OW_LOW()	ONE_WIRE_DQ_SetLow(); ONE_WIRE_DQ_SetDigitalOutput()
#define OW_HIGH()	ONE_WIRE_DQ_SetHigh(); ONE_WIRE_DQ_SetDigitalOutput()

uint8_t one_wire_reset()
{
	uint8_t presence, temp;

	OW_FLOAT();
	OW_LOW();
	__delay_us(600);
	OW_FLOAT();
	__delay_us(80);
	presence = ONE_WIRE_DQ_GetValue();
	__delay_us(600);
	temp = ONE_WIRE_DQ_GetValue();

	return !temp ? 2 : presence;
}

void one_wire_write_bit(uint8_t b)
{
	OW_FLOAT();
	OW_LOW();
	__delay_us(5);
	if (b)
		OW_FLOAT();
	__delay_us(60);
	OW_HIGH();
	__delay_us(5);
}

void one_wire_write_byte(uint8_t b)
{
	for (uint8_t i = 0; i < 8; ++i) {
		one_wire_write_bit(b & 0x01);
		b >>= 1;
	}
}

uint8_t one_wire_read_bit()
{
	uint8_t data;

	OW_FLOAT();
	OW_LOW();
	__delay_us(10);
	OW_FLOAT();
	__delay_us(10);
	data = ONE_WIRE_DQ_GetValue();
	__delay_us(40);

	return data;
}

uint8_t one_wire_read_byte()
{
	uint8_t data = 0;

	for (uint8_t i = 0; i < 8; i++) {
		if (one_wire_read_bit())
			data |= (0x01 << i);
	}

	return data;
}

