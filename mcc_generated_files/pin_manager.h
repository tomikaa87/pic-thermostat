/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65.2
        Device            :  PIC16F18426
        Driver Version    :  2.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45
        MPLAB 	          :  MPLAB X 4.15	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set KDRV_1 aliases
#define KDRV_1_TRIS                 TRISAbits.TRISA0
#define KDRV_1_LAT                  LATAbits.LATA0
#define KDRV_1_PORT                 PORTAbits.RA0
#define KDRV_1_WPU                  WPUAbits.WPUA0
#define KDRV_1_OD                   ODCONAbits.ODCA0
#define KDRV_1_ANS                  ANSELAbits.ANSA0
#define KDRV_1_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define KDRV_1_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define KDRV_1_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define KDRV_1_GetValue()           PORTAbits.RA0
#define KDRV_1_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define KDRV_1_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define KDRV_1_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define KDRV_1_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define KDRV_1_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define KDRV_1_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define KDRV_1_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define KDRV_1_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set KDRV_2 aliases
#define KDRV_2_TRIS                 TRISAbits.TRISA1
#define KDRV_2_LAT                  LATAbits.LATA1
#define KDRV_2_PORT                 PORTAbits.RA1
#define KDRV_2_WPU                  WPUAbits.WPUA1
#define KDRV_2_OD                   ODCONAbits.ODCA1
#define KDRV_2_ANS                  ANSELAbits.ANSA1
#define KDRV_2_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define KDRV_2_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define KDRV_2_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define KDRV_2_GetValue()           PORTAbits.RA1
#define KDRV_2_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define KDRV_2_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define KDRV_2_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define KDRV_2_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define KDRV_2_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define KDRV_2_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define KDRV_2_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define KDRV_2_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set KDRV_3 aliases
#define KDRV_3_TRIS                 TRISAbits.TRISA2
#define KDRV_3_LAT                  LATAbits.LATA2
#define KDRV_3_PORT                 PORTAbits.RA2
#define KDRV_3_WPU                  WPUAbits.WPUA2
#define KDRV_3_OD                   ODCONAbits.ODCA2
#define KDRV_3_ANS                  ANSELAbits.ANSA2
#define KDRV_3_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define KDRV_3_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define KDRV_3_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define KDRV_3_GetValue()           PORTAbits.RA2
#define KDRV_3_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define KDRV_3_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define KDRV_3_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define KDRV_3_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define KDRV_3_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define KDRV_3_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define KDRV_3_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define KDRV_3_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set KRD_1 aliases
#define KRD_1_TRIS               TRISAbits.TRISA3
#define KRD_1_PORT               PORTAbits.RA3
#define KRD_1_WPU                WPUAbits.WPUA3
#define KRD_1_GetValue()           PORTAbits.RA3
#define KRD_1_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define KRD_1_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define KRD_1_SetPullup()      do { WPUAbits.WPUA3 = 1; } while(0)
#define KRD_1_ResetPullup()    do { WPUAbits.WPUA3 = 0; } while(0)

// get/set PIN_RELAY_CTRL aliases
#define PIN_RELAY_CTRL_TRIS                 TRISAbits.TRISA4
#define PIN_RELAY_CTRL_LAT                  LATAbits.LATA4
#define PIN_RELAY_CTRL_PORT                 PORTAbits.RA4
#define PIN_RELAY_CTRL_WPU                  WPUAbits.WPUA4
#define PIN_RELAY_CTRL_OD                   ODCONAbits.ODCA4
#define PIN_RELAY_CTRL_ANS                  ANSELAbits.ANSA4
#define PIN_RELAY_CTRL_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define PIN_RELAY_CTRL_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define PIN_RELAY_CTRL_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define PIN_RELAY_CTRL_GetValue()           PORTAbits.RA4
#define PIN_RELAY_CTRL_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define PIN_RELAY_CTRL_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define PIN_RELAY_CTRL_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define PIN_RELAY_CTRL_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define PIN_RELAY_CTRL_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define PIN_RELAY_CTRL_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define PIN_RELAY_CTRL_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define PIN_RELAY_CTRL_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set ONE_WIRE_DQ aliases
#define ONE_WIRE_DQ_TRIS                 TRISAbits.TRISA5
#define ONE_WIRE_DQ_LAT                  LATAbits.LATA5
#define ONE_WIRE_DQ_PORT                 PORTAbits.RA5
#define ONE_WIRE_DQ_WPU                  WPUAbits.WPUA5
#define ONE_WIRE_DQ_OD                   ODCONAbits.ODCA5
#define ONE_WIRE_DQ_ANS                  ANSELAbits.ANSA5
#define ONE_WIRE_DQ_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define ONE_WIRE_DQ_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define ONE_WIRE_DQ_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define ONE_WIRE_DQ_GetValue()           PORTAbits.RA5
#define ONE_WIRE_DQ_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define ONE_WIRE_DQ_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define ONE_WIRE_DQ_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define ONE_WIRE_DQ_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define ONE_WIRE_DQ_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define ONE_WIRE_DQ_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define ONE_WIRE_DQ_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define ONE_WIRE_DQ_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set I2C_SCL aliases
#define I2C_SCL_TRIS                 TRISCbits.TRISC0
#define I2C_SCL_LAT                  LATCbits.LATC0
#define I2C_SCL_PORT                 PORTCbits.RC0
#define I2C_SCL_WPU                  WPUCbits.WPUC0
#define I2C_SCL_OD                   ODCONCbits.ODCC0
#define I2C_SCL_ANS                  ANSELCbits.ANSC0
#define I2C_SCL_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define I2C_SCL_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define I2C_SCL_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define I2C_SCL_GetValue()           PORTCbits.RC0
#define I2C_SCL_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define I2C_SCL_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define I2C_SCL_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define I2C_SCL_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define I2C_SCL_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define I2C_SCL_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define I2C_SCL_SetAnalogMode()      do { ANSELCbits.ANSC0 = 1; } while(0)
#define I2C_SCL_SetDigitalMode()     do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set I2C_SDA aliases
#define I2C_SDA_TRIS                 TRISCbits.TRISC1
#define I2C_SDA_LAT                  LATCbits.LATC1
#define I2C_SDA_PORT                 PORTCbits.RC1
#define I2C_SDA_WPU                  WPUCbits.WPUC1
#define I2C_SDA_OD                   ODCONCbits.ODCC1
#define I2C_SDA_ANS                  ANSELCbits.ANSC1
#define I2C_SDA_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define I2C_SDA_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define I2C_SDA_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define I2C_SDA_GetValue()           PORTCbits.RC1
#define I2C_SDA_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define I2C_SDA_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define I2C_SDA_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define I2C_SDA_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define I2C_SDA_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define I2C_SDA_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define I2C_SDA_SetAnalogMode()      do { ANSELCbits.ANSC1 = 1; } while(0)
#define I2C_SDA_SetDigitalMode()     do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set KRD_2 aliases
#define KRD_2_TRIS                 TRISCbits.TRISC2
#define KRD_2_LAT                  LATCbits.LATC2
#define KRD_2_PORT                 PORTCbits.RC2
#define KRD_2_WPU                  WPUCbits.WPUC2
#define KRD_2_OD                   ODCONCbits.ODCC2
#define KRD_2_ANS                  ANSELCbits.ANSC2
#define KRD_2_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define KRD_2_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define KRD_2_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define KRD_2_GetValue()           PORTCbits.RC2
#define KRD_2_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define KRD_2_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define KRD_2_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define KRD_2_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define KRD_2_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define KRD_2_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define KRD_2_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define KRD_2_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set KRD_3 aliases
#define KRD_3_TRIS                 TRISCbits.TRISC3
#define KRD_3_LAT                  LATCbits.LATC3
#define KRD_3_PORT                 PORTCbits.RC3
#define KRD_3_WPU                  WPUCbits.WPUC3
#define KRD_3_OD                   ODCONCbits.ODCC3
#define KRD_3_ANS                  ANSELCbits.ANSC3
#define KRD_3_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define KRD_3_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define KRD_3_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define KRD_3_GetValue()           PORTCbits.RC3
#define KRD_3_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define KRD_3_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define KRD_3_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define KRD_3_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define KRD_3_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define KRD_3_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define KRD_3_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define KRD_3_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()               do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()                do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()                do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()              PORTCbits.RC4
#define RC4_SetDigitalInput()       do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()      do { TRISCbits.TRISC4 = 0; } while(0)
#define RC4_SetPullup()             do { WPUCbits.WPUC4 = 1; } while(0)
#define RC4_ResetPullup()           do { WPUCbits.WPUC4 = 0; } while(0)
#define RC4_SetAnalogMode()         do { ANSELCbits.ANSC4 = 1; } while(0)
#define RC4_SetDigitalMode()        do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set RC5 procedures
#define RC5_SetHigh()               do { LATCbits.LATC5 = 1; } while(0)
#define RC5_SetLow()                do { LATCbits.LATC5 = 0; } while(0)
#define RC5_Toggle()                do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define RC5_GetValue()              PORTCbits.RC5
#define RC5_SetDigitalInput()       do { TRISCbits.TRISC5 = 1; } while(0)
#define RC5_SetDigitalOutput()      do { TRISCbits.TRISC5 = 0; } while(0)
#define RC5_SetPullup()             do { WPUCbits.WPUC5 = 1; } while(0)
#define RC5_ResetPullup()           do { WPUCbits.WPUC5 = 0; } while(0)
#define RC5_SetAnalogMode()         do { ANSELCbits.ANSC5 = 1; } while(0)
#define RC5_SetDigitalMode()        do { ANSELCbits.ANSC5 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/