/*
    This file is part of pic-thermostat.

    pic-thermostat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pic-thermostat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pic-thermostat.  If not, see <http://www.gnu.org/licenses/>.

    Author: Tamas Karpati
    Created on 2017-01-03
*/

#include "cli.h"
#include "mcc_generated_files/mcc.h"
#include "clock.h"
#include "heat_ctl.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PARAM_BUF_SIZE 16

//#define ENABLE_DEBUG

// From wifi-back-end project
typedef enum Command
{
	SetTime = 'A',
	IncrementTemperature,
	DecrementTemperature,
	ActivateBoost,
	DeactivateBoost,
	SetMode,
	SetTargetTemperature,
	SetNightTimeTemperature,
	SetDaytimeTemperature
};

struct {
	enum {
		PS_INIT,
		PS_READ_COMMAND,
		PS_READ_PARAM
	} state;

	char command;
	char param[PARAM_BUF_SIZE];
	uint8_t param_ptr;
} process_state;

static void process_input(char data);
static uint8_t is_valid_command(char data);
static uint8_t is_leading_char(char data);
static uint8_t is_trailing_char(char data);
static void process_command();

static int8_t cmd_set_clock();
static int8_t cmd_increment_temperature();
static int8_t cmd_decrement_temperature();
static int8_t cmd_activate_boost();
static int8_t cmd_deactivate_boost();
static int8_t cmd_set_mode();
static int8_t cmd_set_target_temperature();
static int8_t cmd_set_night_time_temperature();
static int8_t cmd_set_daytime_temperature();

void cli_init()
{
	process_state.state = PS_INIT;
}

void cli_task()
{
	while (EUSART1_DataReady) {
		process_input((char)EUSART1_Read());
	}
}

void cli_update_backend()
{
	/*
	*	Thermostat packet format
	*	;;<active temp 0-ffff>:
	*	<current temp 0-ffff>:
	*	<daytime temp 0-ffff>:
	*	<night time temp 0-ffff>:
	*	<boost state 0-1>:
	*	<heating state 0-1>:
	*	<boost remaining secs 0-ffffffff>:
	*	<mode 0-2>:
	*	<next switch state 0-1>:
	*	<next switch weekday 0-ff>:
	*	<next switch state 0-ff>:
	*	<next switch state 0-ff>:
	*	<time synced 0-1>;
	*/

	printf(";;");

	printf("%x", heatctl_target_temp());
	printf(":");

	printf("%x", heatctl_current_temp());
	printf(":");

	printf("%x", heatctl_daytime_temp());
	printf(":");

	printf("%x", heatctl_night_time_temp());
	printf(":");

	printf("%u", heatctl_is_boost_active() ? 1 : 0);
	printf(":");

	printf("%u", heatctl_is_active() ? 1 : 0);
	printf(":");

	printf("%x", heatctl_boost_remaining_secs());
	printf(":");

	printf("%u", heatctl_mode());
	printf(":");

	struct heatctl_next_switch ns = heatctl_next_state();
	printf("%x", ns.state);
	printf(":");
	printf("%x", ns.weekday);
	printf(":");
	printf("%x", ns.hour);
	printf(":");
	printf("%x", ns.minute);
	printf(":");

	printf("%d", clock_synced);

	printf(";");
}

void cli_log(const char* msg)
{
	printf("##%s#", msg);
}

static void process_input(char data)
{
	switch (process_state.state) {
	case PS_INIT:
		if (is_leading_char(data)) {
			process_state.state = PS_READ_COMMAND;
			process_state.command = 0;
		}

		break;

	case PS_READ_COMMAND:
		// Skip leading chars if there are more of them
		if (is_leading_char(data)) {
			break;
		}

		if (!is_valid_command(data)) {
			process_state.state = PS_INIT;
			break;
		}

		process_state.state = PS_READ_PARAM;
		process_state.command = data;
		process_state.param_ptr = 0;
		memset(process_state.param, 0, PARAM_BUF_SIZE);
		break;

	case PS_READ_PARAM:
		if (is_trailing_char(data)) {
			process_state.state = PS_INIT;
			process_command();
			break;
		}

		if (!isalnum(data)) {
			process_state.state = PS_INIT;
#ifdef ENABLE_DEBUG
			printf("# Invalid param char (!isalnum)\r\n");
#endif
			break;
		}

		if (process_state.param_ptr == PARAM_BUF_SIZE) {
			process_state.state = PS_INIT;
#ifdef ENABLE_DEBUG
			printf("# Param buffer full (%u)\r\n", PARAM_BUF_SIZE);
#endif
			break;
		}

		process_state.param[process_state.param_ptr++] = (char)data;
		break;
	}
}

static uint8_t is_valid_command(char data)
{
	return data >= 'A' && data <= 'Z';
}

static uint8_t is_leading_char(char data)
{
	return data == ';';
}

static uint8_t is_trailing_char(char data)
{
	return data == ';';
}

static void process_command()
{
	if (!is_valid_command(process_state.command))
		return;

#ifdef ENABLE_DEBUG
	printf("# Command: %c\r\n", process_state.command);
#endif

	int8_t cmd_result = -1;

	switch (process_state.command) {
	case SetTime:
		cmd_result = cmd_set_clock();
		if (cmd_result == 0)
			clock_synced = true;
		break;

	case IncrementTemperature:
		cmd_result = cmd_increment_temperature();
		break;

	case DecrementTemperature:
		cmd_result = cmd_decrement_temperature();
		break;

	case ActivateBoost:
		cmd_result = cmd_activate_boost();
		break;

	case DeactivateBoost:
		cmd_result = cmd_deactivate_boost();
		break;

	case SetMode:
		cmd_result = cmd_set_mode();
		break;

	case SetTargetTemperature:
		cmd_result = cmd_set_target_temperature();
		break;

	case SetNightTimeTemperature:
		cmd_result = cmd_set_night_time_temperature();
		break;

	case SetDaytimeTemperature:
		cmd_result = cmd_set_daytime_temperature();
		break;
	}

	if (cmd_result == -1) {
		printf("##No such command: %d#", process_state.command);
	} else if (cmd_result != 0) {
		printf("##Command failed: %d#", process_state.command);
	}

#ifdef ENABLE_DEBUG
	if (cmd_result == -1) {
		printf(";NOCMD\r\n");
	} else if (cmd_result == 0) {
		printf(";OK\r\n");
	} else
		printf(";ERROR\r\n");
#endif
}

static int8_t cmd_set_clock()
{
	if (process_state.param_ptr == 0) {
#ifdef ENABLE_DEBUG
		printf("# set_clock: empty param\r\n");
#endif
		return 1;
	}

	char* end;
	clock_epoch = strtol(process_state.param, &end, 10);
	return 0;
}

static int8_t cmd_increment_temperature()
{
	heatctl_inc_target_temp();
	return 0;
}

static int8_t cmd_decrement_temperature()
{
	heatctl_dec_target_temp();
	return 0;
}

static int8_t cmd_activate_boost()
{
	if (heatctl_is_boost_active())
		heatctl_extend_boost();
	else
		heatctl_activate_boost();

	return 0;
}

static int8_t cmd_deactivate_boost()
{
	heatctl_deactivate_boost();
	return 0;
}

static int8_t cmd_set_mode()
{
	if (process_state.param_ptr == 0)
	{
#ifdef ENABLE_DEBUG
		printf("# set_mode: empty param\r\n");
#endif
		return 1;
	}

	char* end;
	uint8_t mode = strtol(process_state.param, &end, 10);
	heatctl_set_mode((heatctl_mode_t)mode);

	return 0;
}

static int8_t cmd_set_target_temperature()
{
	if (process_state.param_ptr == 0)
	{
#ifdef ENABLE_DEBUG
		printf("# set_target_temperature: empty param\r\n");
#endif
		return 1;
	}

	// 'param' contains tenths of degrees, min. 80, max. 3200

	char* end;
	tenths_of_degrees_t value = strtol(process_state.param, &end, 10);
	heatctl_set_target_temp(value);

	return 0;
}

static int8_t cmd_set_night_time_temperature()
{
	if (process_state.param_ptr == 0)
	{
#ifdef ENABLE_DEBUG
		printf("# set_night_time_temperature: empty param\r\n");
#endif
		return 1;
	}

	// 'param' contains tenths of degrees, min. 80, max. 3200

	char* end;
	tenths_of_degrees_t value = strtol(process_state.param, &end, 10);
	heatctl_set_night_time_temp(value);

	return 0;
}

static int8_t cmd_set_daytime_temperature()
{
	if (process_state.param_ptr == 0)
	{
#ifdef ENABLE_DEBUG
		printf("# set_daytime_temperature: empty param\r\n");
#endif
		return 1;
	}

	// 'param' contains tenths of degrees, min. 80, max. 3200

	char* end;
	tenths_of_degrees_t value = strtol(process_state.param, &end, 10);
	heatctl_set_daytime_temp(value);

	return 0;
}